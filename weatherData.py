'''
Functions to handle and cache the data received from the SRGSSR weather API.

@package infodisplay
'''

import json
import xml.etree.ElementTree as ET
import config
import requests
from requests.auth import HTTPBasicAuth
from datetime import date, datetime, timedelta
import dateutil.parser

def getCurrentDay():
    dayTemps = {
        "tempLow": "-",
        "tempHigh": "-" 
    }
    
    weatherData = loadSRGSSRData(config.currentForecast)

    if weatherData is not None:
        currentDay = weatherData.get("current_day")
        dayValues = currentDay.get("values")

        for dayValue in dayValues:
            if "ttn" in dayValue:
                dayTemps["tempLow"] = dayValue["ttn"] + "°"
            if "ttx" in dayValue:
                dayTemps["tempHigh"] = dayValue["ttx"] + "°"

    return dayTemps

def getCurrentHour():
    currentHour = { 
        "tempNow": "-",
        "symbolHour": 0,    
        "rainProb": "-",
        "rainPrecip": "-",
        "windMax3h": "-",
        "windMean1h": "-",
        "windDir": 0,
        "date": "1970-01-01 00:00:00"
    }

    weatherData = loadSRGSSRData(config.currentForecast)

    if weatherData is not None:
        currentHourData = weatherData.get("current_hour")
        currentHour["date"] = currentHourData[0].get("date")
        hourValues = currentHourData[0].get("values")
    
        for hourValue in hourValues:
            if "smb3" in hourValue:
                currentHour["symbolHour"] = hourValue["smb3"]
            if "ttt" in hourValue:
                currentHour["tempNow"] = hourValue["ttt"]
            if "pr3" in hourValue:
                currentHour["rainProb"] = hourValue["pr3"] + "%"
            if "rr3" in hourValue:
                currentHour["rainPrecip"] = hourValue["rr3"] + "mm"
            if "fff" in hourValue:
                currentHour["windMean1h"] = hourValue["fff"] + "km"         
            if "ffx3" in hourValue:
                currentHour["windMax3h"] = hourValue["ffx3"] + "km"
            if "ddd" in hourValue:
                currentHour["windDir"] = int(hourValue["ddd"]) if hourValue["ddd"] != "" else 0

    return currentHour

def get7DayWeather():
    return loadSRGSSRData(config.weeksForecastbyId)


def lookupWeatherSymbol(symNo):
    iconName = 'notavailable'
    
    tree = ET.parse(config.iconMappingXML)
    root = tree.getroot()
    
    for icon in root.findall("row"):
        if symNo == icon.find("Code").text:
            iconName = icon.find("Code_icon").text
    
    return iconName


def getLargeSymbol(symNo):
    return config.iconPathLarge + lookupWeatherSymbol(symNo) + ".png"

def getSmallSymbol(symNo):
    return config.iconPathSmall + lookupWeatherSymbol(symNo) + ".png"

def loadSRGSSRData(whichData):
    tmpFile = 'download/' + whichData + '.tmp.json'
    weatherJSON = None
    weatherData = None
    
    try:
        with open(tmpFile, "r") as json_file:
            weatherData = json.load(json_file)
    except:
        print("could not load data from cached file")
        
    date_now = datetime.now() 
    fetchDate = datetime.fromtimestamp(1)
    
    if "fetchDate" in weatherData:
        fetchDate = dateutil.parser.parse(weatherData["fetchDate"])
    
    if fetchDate + timedelta(minutes=15) < date_now:
        weatherJSON = fetchSRGSSRData(whichData)
        if weatherJSON is not None:
            weatherData = json.loads(weatherJSON) 
            weatherData["fetchDate"] = date_now.isoformat()

            with open(tmpFile, "w") as json_file:
                json.dump(weatherData ,json_file)
        
    return weatherData

def fetchSRGSSRData(whichData):
    token_url = config.srgssrApiAuthorizeURL
    srg_endpoint_url = config.srgssrapiEndPoints.get(whichData)

    #client (application) credentials on apim.byu.edu
    client_id = config.srgssrApiConsumer
    client_secret = config.srgssrApiSecret

    api_call_res_content = None
    try: 
        access_token_response = requests.post(token_url, verify=False, allow_redirects=False, auth=(client_id, client_secret), timeout=30)
        #print (access_token_response.request.headers)
        #print (access_token_response.headers)
        #print (access_token_response.text)
        tokens = json.loads(access_token_response.text)

        print("access token: " + tokens['access_token'])

        # srgssr API requires the positional parameters as URL parameters
        params = {'latitude': config.latitude, 'longitude': config.longitude}
        api_call_headers = {'Authorization': 'Bearer ' + tokens['access_token']}
        api_call_response = requests.get(srg_endpoint_url, headers=api_call_headers, verify=False, params=params, timeout=30)
        api_call_res_content = api_call_response.text
    except:
        print("error while fetching data from the SRG API")
        return None

    return api_call_res_content


        
