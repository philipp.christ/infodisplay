'''
The e-paper update workings.
Based on the example provided for the waveshare epaper display library

@package infodisplay
'''

import sys
sys.path.append("./lib") # Adds lib folder in this directory to sys

import epd7in5_V2
import infodisplay
from PIL import Image




def printToDisplay():
    HBlackImage = infodisplay.drawInfoDisplay(epd7in5_V2.EPD_WIDTH, epd7in5_V2.EPD_HEIGHT)
    #HBlackImage = HBlackImage.rotate(180)

    epd = epd7in5_V2.EPD() # get the display
    epd.init()           # initialize the display
    print("Clear...")    # prints to console, not the display, for debugging
    epd.Clear()      # clear the display
    
    epd.display(epd.getbuffer(HBlackImage))
    
printToDisplay()
