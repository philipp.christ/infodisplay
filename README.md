# infodisplay

Render monochrome infodisplay type layouts

Currently it specifically targets the Waveshare 7.5inV2 e-paper.

![Logo](img/framed.jpg)


Currently implemented data sources:

- [Open-Data-Plattform Mobilität Schweiz](https://opentransportdata.swiss/)
- [Stadt Zürich Open Data/Public Pool water temperatures](https://data.stadt-zuerich.ch/dataset/wassertemperaturen-freibaeder)
- calendar library
- holidays library
- discordian date library
- icalevents library

## Dependencies

- [waveshare e-paper drivers](https://github.com/waveshare/e-Paper)
- [ioBroker.swiss-weather-api by baerengraben](https://github.com/baerengraben/ioBroker.swiss-weather-api/) as a subproject used for the weather images
- dependencies: see requirements.txt


## Usage

```python3 update.py```

Update the e-paper display with the current layout/data

```python3 prototype.py``` 

Update **prototype.png** for rapid review of changes made

The layout is organised in panels so it should not be too difficult to organise or reuse for different versions.

The panels are layed out using coordinates and their width/height in infodisplay.py.



## Contributing
If you have any suggestions, PRs, or colaborations please get in contact.

## License
[MIT](https://choosealicense.com/licenses/mit/)

Copyright (c) 2020 Philipp Christ <pch@mimir.ch>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
