'''
A simple display of the current days discordian date.


@package infodisplay
'''

from ddate.base import DDate
import re
from PIL import Image, ImageDraw, ImageFont
import IDFont

def drawDDate(width, height):
    DDateImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(DDateImage)
    
    ddate = str(DDate())
    match = re.search("Today is (.*), the (.* day of) (.*) in the (YOLD.*)", ddate)
    ddate = match.group(1) + "\n" + match.group(2) + "\n" + match.group(3) + "\n" + match.group(4)
    #last_update = str(DDate()) + " " + datetime.now().strftime("%H:%M")
    twidth, theight = IDFont.hack18.getsize(ddate)
    
    draw.text((0,0), ddate , font = IDFont.hack18, fill = 0)

    return DDateImage
