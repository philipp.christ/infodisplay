'''
Handling of the opentransport trias stopevent API Data.

@package infodisplay
'''

import config
import requests
import xml.etree.ElementTree as ET
import dateutil.parser
from pytz import timezone
import traceback
import json

from datetime import date, datetime, timedelta


def getDepartures():
    
    departures = []

    try:
        departuresXML = __fetchDeparturesXML()
        tree = ET.fromstring(departuresXML)
        ns = {'trias': 'http://www.vdv.de/trias'}
        # 2022-05-14T16:01:00Z
        triasTimeFormat = "%Y-%m-%dT%H:%M:%SZ"
        

        for stopEvent in tree.findall(".//trias:StopEvent", ns):
            departureTimeFull = __getTextFromFirstTag(stopEvent, ".//trias:TimetabledTime", ns)
            estimatedTimeFull = __getTextFromFirstTag(stopEvent, ".//trias:EstimatedTime", ns)

            departureTime = "--:--"
            delay = ""
            try:
                departureDateTime = datetime.strptime(departureTimeFull, triasTimeFormat).replace(tzinfo=timezone("UTC"))
                departureTimeLocal = departureDateTime.astimezone(config.localTZ)
                departureTime = departureTimeLocal.strftime("%H:%M")

                try: 
                    estimatedDateTime = datetime.strptime(estimatedTimeFull, triasTimeFormat).replace(tzinfo=timezone("UTC"))
                    estimatedTimeLocal = estimatedDateTime.astimezone(config.localTZ)
                    delayDT = estimatedTimeLocal - departureTimeLocal
                    minutes = int(round(delayDT.total_seconds()/60, 0))
                    if minutes > 0: delay = f"+{minutes}"
                    if minutes < 0: delay = f"{minutes}"
                except:
                    traceback.print_exc()
                    print("could not parse estimatedTime")

            except:
                traceback.print_exc()
                print("could not parse departureTime")

            destinationShort = ""
            destinationFull = __getTextFromFirstTag(stopEvent, ".//trias:DestinationText/trias:Text", ns)
            destParts = destinationFull.partition(",")
            
            if len(destParts[2]) > 0:
                destinationShort = destParts[2] 
            else:
                destinationShort = destinationFull
                
            departure = {
                "line": __getTextFromFirstTag(stopEvent, ".//trias:PublishedLineName/trias:Text", ns),
                "destination": destinationShort,
                "departureTime": departureTime,
                "delay": delay
            }
            departures.append(departure)


        #print(json.dumps(departures))

    except Exception as e:
        print("could not parse XML from departuresData")
        traceback.print_exc()

    return departures


def __getTextFromFirstTag(element, path, ns):
    text = ""

    try:
        for item in element.findall(path, ns):
            text = item.text
            break
    except:
        print(f"error while trying to extract field {path}")
    
    return text


def __fetchDeparturesXML():
    departuresXML = None

    try:
        req_template = config.idtemplates.get_template(config.departures["postRequestTemplate"])

        queryForTime = datetime.now() + timedelta(minutes=int(config.departures["minutesAdvance"]))

        departuresRequestData = {
            "STOP_REF": config.departures.get("stop"),
            "DEPARTURE_TIME": queryForTime.strftime("%Y-%m-%dT%H:%M:%S"),
            "NUMBER_OF_RESULTS": config.departures.get("numberOfResults")
        }
        postReqBody = req_template.render(departuresRequestData)
    except Exception as e: 
        print("error while preparing request body for departure API query: ")


    try:
        headers = {
            "Authorization": config.departuresSec.get("secret"),
            "Content-Type": "application/XML"
        }

        api_call_response = requests.post(config.departures.get("transportURL"), data=postReqBody, headers=headers)
        departuresXML = api_call_response.content
    except Exception as e: 
        print("error while getting departure data from API endpoint: ")
        
    return departuresXML
