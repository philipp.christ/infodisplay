'''
Contains all date/calendar/holiday related drawing functions.

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
import calendar
from datetime import date
import holidays 
import datetime
import IDFont

    
def drawCalendar(width, height):
    CalendarImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CalendarImage) # Create draw object and pass in the image layer we want to work with (HBlackImage)

    draw.rectangle([(0,0),(width,height)], fill = 0)

    dateDisp = drawDateDisp(200, 180)
    CalendarImage.paste(dateDisp, (10, 10))        
    
    draw.line([(10,175),(210,175)], fill = 100, width = 1)

    ncalImg = drawTextCalendar(200, 200)
    CalendarImage.paste(ncalImg, (10, 185))        

    draw.line([(10,350),(210,350)], fill = 100, width = 1)
    
    holidayImg = drawHolidays(200, 110)
    CalendarImage.paste(holidayImg, (10, height-holidayImg.height-10))    
    
    return CalendarImage


def drawDateDisp(width, height):
    CurrentImage = Image.new('1', (int(width), int(height)), 0)
    draw = ImageDraw.Draw(CurrentImage)

    today = date.today()
    textToCenter = today.strftime("%A")
    textwidth, textheight = draw.textsize(textToCenter, font= IDFont.roboto30)
    draw.text((width/2-(textwidth/2), 0), textToCenter, font = IDFont.roboto30, fill = 100)
    
    textToCenter = today.strftime("%d")
    textwidth, textheight = draw.textsize(textToCenter, font= IDFont.robotoCondensedBold96)
    draw.text((width/2-(textwidth/2), 25), textToCenter, font = IDFont.robotoCondensedBold96, fill = 100)
    
    textToCenter = today.strftime("%B")
    textwidth, textheight = draw.textsize(textToCenter, font= IDFont.roboto30)
    draw.text((width/2-(textwidth/2), 125), textToCenter , font = IDFont.roboto30, fill = 100)

    return CurrentImage


def drawTextCalendar(width, height):
    CurrentImage = Image.new('1', (int(width), int(height)), 0)
    draw = ImageDraw.Draw(CurrentImage)
    
    today = date.today()
    tc= calendar.TextCalendar(firstweekday=0)
    ncal = tc.formatmonth(today.year, today.month)
    twidth, theight = draw.multiline_textsize(ncal, IDFont.hack16)
    draw.text((0,0), ncal, font = IDFont.hack16, fill = 100)
    
    return CurrentImage


def drawHolidays(width, height):
    CurrentImage = Image.new('1', (int(width), int(height)), 0)
    draw = ImageDraw.Draw(CurrentImage)

    today = date.today()
    ch_holidays = holidays.Switzerland(years = today.year, prov='ZH') # Select country 
    holidayY = 0
    for datum, name in sorted(ch_holidays.items()):
        if datum < date.today():
            continue
        if holidayY > height - 20:
            break            

        dateText = datum.strftime("%d.%m. - ") + name
        twidth, theight = IDFont.robotoCondensed18.getsize(dateText)
        draw.text((0,holidayY), dateText, font = IDFont.robotoCondensed18, fill = 100)
        holidayY += 20
        
    return CurrentImage
    
