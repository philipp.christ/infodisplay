'''
Stores the most common configurable values used through the project.

@package infodisplay
'''

import json
from jinja2 import Environment, FileSystemLoader, select_autoescape
from pytz import timezone

# initialize the jinja2 template loader
idtemplates = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)


infodisplay = {}
# parse infodisplay.json
with open('infodisplay.json', 'r') as f:
    infodisplay = json.load(f).get("infodisplay", {})

position = infodisplay.get('position', {})
localisation = infodisplay.get('localisation', {})
weathericons = infodisplay.get('weatherIcons', {})
eventCalendar = infodisplay.get('eventCalendar', {})
badi = infodisplay.get('badi', {})
departures = infodisplay.get('departures', {})

localTZ = timezone(localisation.get("timezone"))

secrets = {}
# parse secrets.json
with open('secrets.json', 'r') as f:
    secrets = json.load(f).get("secrets", {})

eventCalendarSec = secrets.get("eventCalendar", {})
departuresSec = secrets.get("departures", {})




