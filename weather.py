'''
The functions to draw the various weather information pannels (current, 3h forecast, 5d forecast)

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
import IDFont
import weatherData
from datetime import date, datetime
import config
    
    
def stripedRect(draw, x, y, w, h):
    line = y
    end = y + h
    while line <= end:
        draw.line([(x,line), (x+w, line)])
        line += 2
        
        
def getCurrentSymbol(currentSymbol, width, height):
    CurrentImage = Image.new('1', (width, height), 255)
    weather3h = Image.open(currentSymbol)
    weather3h = weather3h.resize((width, height))
    CurrentImage.paste(weather3h, (0, 0), weather3h)
    return CurrentImage

def getCurrentTemps(tempHigh, tempLow, tempNow, width, height):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage) 
    
    twidth, theight = IDFont.hack36bold.getsize(tempNow)
    spacing = 5
    draw.text((width/2-twidth/2,height/2-theight/2), tempNow, font = IDFont.hack36bold, fill = 0)
    
    twidth, theight = IDFont.hack18.getsize(tempHigh)
    draw.text((width/2-twidth/2,spacing), tempHigh, font = IDFont.hack18, fill = 0)

    twidth, theight = IDFont.hack18.getsize(tempLow)
    draw.text((width/2-twidth/2,height-theight-spacing), tempLow, font = IDFont.hack18, fill = 0)
    
    return CurrentImage

def getCurrentCond(rainSymbol, rota, text1, text2, width, height):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage) 
    condImg = Image.open(rainSymbol)
    
    secondrowY = height / 2
    secondColX = width - condImg.width +5
    
    condImg = condImg.rotate(rota)
    CurrentImage.paste(condImg, (width-secondColX, 0), condImg)

    spacing = 2
    twidth, theight = IDFont.robotoCondensed18.getsize(text1)
    draw.text((secondColX-twidth+5,secondrowY-theight-spacing), text1, font = IDFont.robotoCondensed18, fill = 0)

    twidth, theight = IDFont.robotoCondensed18.getsize(text2)
    draw.text((secondColX-twidth+5,secondrowY+spacing), text2, font = IDFont.robotoCondensed18, fill = 0)
    
    return CurrentImage

    
def drawCurrenWeather(width, height):
    CurrentImage = Image.new('1', (width, height), 255)

    dayTemps = weatherData.getCurrentDay()
    currentHour = weatherData.getCurrentHour()
         

    currentWeatherSym = weatherData.getLargeSymbol(currentHour.get("symbolHour"))
    weather3h = getCurrentSymbol(currentWeatherSym, 120, 120)
    CurrentImage.paste(weather3h, (0, 0))
    
    tempsNow = getCurrentTemps(dayTemps.get("tempHigh"), dayTemps.get("tempLow"), currentHour.get("tempNow"),100, 100)
    CurrentImage.paste(tempsNow, (int(width/2-tempsNow.width/2), 5))

    rainSym = config.iconPathSmall + 'wi-umbrella.png'
    rainNow = getCurrentCond(rainSym, 0, currentHour.get("rainProb"), currentHour.get("rainPrecip"), 120, 60)
    CurrentImage.paste(rainNow, (width-120, 0))
    
    windSym = config.iconPathSmall + 'wi-direction-up.png'
    windNow = getCurrentCond(windSym, currentHour.get("windDir"), currentHour.get("windMean1h"), currentHour.get("windMax3h"), 120, 60)
    CurrentImage.paste(windNow, (width-120, 55))
    
    return CurrentImage


def drawForecastPane(daysName, daysMax, daysMin, daysSym, width, height):
    CurrentImage = Image.new('1', (int(width), int(height)), 255)
    draw = ImageDraw.Draw(CurrentImage)
          
    weatherND = Image.open(weatherData.getSmallSymbol(daysSym))
    CurrentImage.paste(weatherND, (width-weatherND.width, 0), weatherND)

    twidth, theight = IDFont.hack18.getsize(daysName)
    draw.text((int((width-weatherND.width)/2-twidth/2),0), daysName, font = IDFont.hack18bold, fill = 0)

    twidth, theight = IDFont.hack18.getsize(daysMax)
    draw.text((int((width-weatherND.width)/2-twidth/2),int(height/3)), daysMax, font = IDFont.hack18, fill = 0)

    twidth, theight = IDFont.hack18.getsize(daysMin)
    draw.rectangle([(3,int(height/3*2)+1),(width-weatherND.width-3,height)], fill = 0)
    draw.text((int((width-weatherND.width)/2-twidth/2),int(height/3*2)), daysMin, font = IDFont.hack18, fill = 100)
    
    return CurrentImage

    

def drawForecast(width, height):
    ForecastImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(ForecastImage)
    startX = 0
    startY = 0

    sevenDays = weatherData.get7DayWeather().get("7days")
    
    for day in sevenDays:
        fcDate = datetime.strptime(day.get("date"),'%Y-%m-%d')
        if datetime.now() >= fcDate:
            continue
        
        dayName = fcDate.strftime("%a")
        daysSym = "-"
        daysMin = "-"
        daysMax = "-"
        
        for dayValue in day.get("values"):
            if "ttn" in dayValue:
                daysMin = str(round(float(dayValue["ttn"])))
            if "ttx" in dayValue:
                daysMax = str(round(float(dayValue["ttx"])))
            if "smbd" in dayValue:
                daysSym = dayValue["smbd"]
        
        fcPane = drawForecastPane(dayName, daysMax, daysMin, daysSym, 110, 58)
        ForecastImage.paste(fcPane, (startX, startY))
        startX += fcPane.width + 4
    
    return ForecastImage

