'''
Draw a logo followed by the Stadt Zürich Open Data public pool information

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
from datetime import date, datetime
import IDFont
import config
import badiData
import textwrap

def drawBadis(width, height):
    CurrentImage = Image.new('1', (int(width), int(height)), 255)

    swimmerImg = Image.open('img/fontawesome/swimmer-solid.png')
    CurrentImage.paste(swimmerImg, (int(width/2-swimmerImg.width/2), 5), swimmerImg)

    badiHeight = height-swimmerImg.height
    
    pheight = int(badiHeight / 4) 
    line = 0

    for badi in badiData.getBadiInfo():
        if badi.get('title') in config.badi["ourBadis"]:
            badiImg = drawSingleBadi(width, pheight, badi)
            CurrentImage.paste(badiImg, (0,swimmerImg.height+pheight*line))
            line += 1

    return CurrentImage

def drawSingleBadi(width, height, infos):
    CurrentImage = Image.new('1', (int(width), int(height)), 255)
    draw = ImageDraw.Draw(CurrentImage) 
    
    badiName = infos.get("title")
    twidth, theight = IDFont.robotoCondensedBold18.getsize(badiName)

    # the open/closed information is an undefinded free text which
    # can vary between locations
    # do some wrapping to get as much information on the screen
    # but drop anything but the first two lines
    openClosed = infos.get("openClosedTextPlain")
    openClosed = openClosed if openClosed.find("Saisonende") == -1 else openClosed.replace("geschlossen", "geschl.")
    openClosed1 = openClosed
    openClosed2 = ""
    wrapAt = 100
    owidth, oheight = IDFont.robotoCondensed18.getsize(openClosed1)
    while owidth >= width - 5:
        owidth, oheight = IDFont.robotoCondensed18.getsize(openClosed1)
        openLines = textwrap.wrap(openClosed, width=wrapAt)
        openClosed1 = openLines[0] + " "
        openClosed2 = openLines[1] + " " if len(openLines) > 1 else ""
        wrapAt -= 1
        if wrapAt == 0:
            break # just in case we hit the unwrappable
    
    draw.text((0,5), badiName + "  ", font = IDFont.robotoCondensedBold18, fill = 0)
    draw.text((0,oheight + 5), openClosed1, font = IDFont.robotoCondensed18, fill = 0)
    draw.text((0,oheight*2+5), openClosed2, font = IDFont.robotoCondensed18, fill = 0)
    
    temperature = "-°" if openClosed.find("Saisonende") >= 0 else infos.get("temperatureWater") + "°"
    twidth, theight = IDFont.robotoBold18.getsize(temperature)
    draw.text((width-twidth-10,5), temperature, font = IDFont.robotoBold18, fill = 0)
    
    return CurrentImage
