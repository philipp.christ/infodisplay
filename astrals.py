'''
Calculate and draw the sunrise/sunset and golden hour dates.

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
from datetime import date, datetime
from astral import LocationInfo
from astral.sun import sun, golden_hour, SunDirection
from pytz import timezone
import IDFont
#import weatherData
import config

def riseandfall(riseSym, rise, fall, width, height):
    # single panel containing a start time (rise), symbol (riseSym) and end time (fall)
    CurrentImage = Image.new('1', (int(width), int(height)), 255)
    draw = ImageDraw.Draw(CurrentImage) 

    sunriseImg = Image.open(riseSym)
    CurrentImage.paste(sunriseImg, (int(width/2-sunriseImg.width/2), 5), sunriseImg)

    spacing = 10
    twidth, theight = IDFont.roboto20.getsize(rise)
    draw.text((int(width/2-sunriseImg.width/2-twidth),height/2-theight/2+spacing), rise, font = IDFont.roboto20, fill = 0)    

    twidth, theight = IDFont.roboto20.getsize(fall)
    draw.text((int(width/2+sunriseImg.width/2),height/2-theight/2+spacing), fall, font = IDFont.roboto20, fill = 0)    
    
    return CurrentImage


def drawAstrals(width, height):
    CurrentImage = Image.new('1', (width, height), 255)

    city = LocationInfo(config.localisation["city"], config.localisation["country"], config.localisation["timezone"], config.position["latitude"], config.position["longitude"])

    localTime = config.localTZ.localize(datetime.now())
    sunTimes = sun(city.observer, date=localTime, tzinfo=config.localTZ)
    
    sunrise = sunTimes["sunrise"].strftime("%H:%M")
    sunset = sunTimes["sunset"].strftime("%H:%M")
    sunriseImg = config.weathericons.get("iconPathSmall") + 'wi-sunrise.png'
    sunrisePane = riseandfall(sunriseImg, sunrise, sunset, width, height/2)
    CurrentImage.paste(sunrisePane, (0, 0))


    goldstart, goldend  = golden_hour(city.observer, date=localTime, direction=SunDirection.SETTING, tzinfo=config.localTZ)
    moonriseImg = config.weathericons.get("iconPathSmall") + 'wi-horizon.png'
    moonrisePane = riseandfall(moonriseImg, goldstart.strftime("%H:%M"), goldend.strftime("%H:%M"), width, height/2)
    CurrentImage.paste(moonrisePane, (0, int(height/2)))


    return CurrentImage
