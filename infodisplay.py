'''
The overall layout and drawing of the infodisplay.
Usually called from update.py or prototype.py

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
import calendarInfo
#import weather
import astrals
import config
import IDFont
import departuresPanel
#import discorddate
import eventPanel
import badi
from datetime import datetime, time


def drawInfoDisplay(w, h): 
    HBlackImage = Image.new('1', (w, h), 255)
    draw = ImageDraw.Draw(HBlackImage) # Create draw object and pass in the image layer we want to work with (HBlackImage)
    
    calendarImage = calendarInfo.drawCalendar(220, 480)  
    HBlackImage.paste(calendarImage, (0,0))

    #currentWeather = weather.drawCurrenWeather(350, 120)  
    #HBlackImage.paste(currentWeather, (220,0))
    
    draw.line([(449,10),(449,178)], fill = 0, width = 1)

    # add the time of the last update in case something stops working
    last_update = datetime.now().strftime("%H:%M")
    twidth, theight = IDFont.hack20.getsize(last_update)    
    draw.text((300,10), last_update , font = IDFont.hack20, fill = 0)


    astralsPane = astrals.drawAstrals(220, 110)
    HBlackImage.paste(astralsPane, (220, 50))

    #draw.line([(230,120),(790,120)], fill = 0, width = 1)

    departures = departuresPanel.drawDeparturesPanel(350, 184)  
    HBlackImage.paste(departures, (450,0))
   
    draw.line([(230,184),(790,184)], fill = 0, width = 1)


    badis = badi.drawBadis(220, 280)
    HBlackImage.paste(badis, (230, 190))

    draw.line([(449,190),(449,470)], fill = 0, width = 1)

    eventImg = eventPanel.drawEventPanel(350,280)
    HBlackImage.paste(eventImg, (w-eventImg.width, 190))

    

    return HBlackImage
