'''
The e-paper update may take quite some time (5-6s for the monochrome version.
Calling prototype.py allows more rapid cycles to see how changes affect the layout.

@package infodisplay
'''

import infodisplay
from PIL import Image, ImageDraw, ImageFont

def printToFile():
    HBlackImage = infodisplay.drawInfoDisplay(800, 480)
    HBlackImage.save("prototype.png")
    
    
printToFile()
