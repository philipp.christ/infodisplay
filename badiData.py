'''
Handling of the Zurich Open Data public pool temperatures information.

@package infodisplay
'''

import config
import requests
import xml.etree.ElementTree as ET
import dateutil.parser
from datetime import date, datetime, timedelta


def getBadiInfo():
    tmpFile = 'download/stzh-batchdatadownload_tmp.xml'
    
    badiInfos = None
    date_now = datetime.now()
    fetchDate = datetime.fromtimestamp(1)
    
    try:
        tree = ET.parse(tmpFile)
        root = tree.getroot()
        # <dateformat>EE, dd.MM.yyyy HH:mm</dateformat>
        # dateparser and strptime don't like this format very much, we strip the weekday
        dateWithoutWeekday = root.find("meta").find("dateTimeSent").text[4:]
        fetchDate = datetime.strptime(dateWithoutWeekday, "%d.%m.%Y %H:%M")
    except:
        print("could not get badi infos from cached file")
        
    # the temperatures are not updated very often
    # only fetch the XML if our cache is older than 30 minutes
    if fetchDate + timedelta(minutes=30) < date_now:
        badiXML = fetchBadiXML()
        tree = ET.fromstring(badiXML)
        with open(tmpFile, "w") as cacheFile:
            cacheFile.write(badiXML)

    try:
        badiInfos = [{item.tag: item.text for item in ch} for ch in tree.find("baths").findall("bath")]        
    except:
        print("could not parse XML from badi-API")

    return badiInfos

def fetchBadiXML():
    badiXML = None
    try:
        api_call_response = requests.get(config.badi["badiAPIURL"], verify=False, timeout=30)
        badiXML = api_call_response.text
    except: 
        print("error while getting badi infos from API endpoint")
        
    return badiXML
