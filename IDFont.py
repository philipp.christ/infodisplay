'''
Just a little helper to simply load and get hold of all the fonts in the project
There might be better ways to load and reference all the fonts used.


@package infodisplay
'''

from PIL import ImageFont

robotoCondensed16 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Regular.ttf', 16)
robotoCondensedBold16 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Bold.ttf', 16)
robotoCondensed18 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Regular.ttf', 18)
robotoCondensedBold18 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Bold.ttf', 18)
robotoCondensed24 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Regular.ttf', 24)
robotoCondensed36 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Regular.ttf', 36)
robotoCondensed48 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Regular.ttf', 48)
robotoCondensedBold96 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoCondensed-Bold.ttf', 96)
roboto18 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoTTF/Roboto-Regular.ttf', 18)
robotoBold18 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoTTF/Roboto-Bold.ttf', 18)
roboto20 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoTTF/Roboto-Regular.ttf', 20)
roboto24 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoTTF/Roboto-Regular.ttf', 24)
roboto30 = ImageFont.truetype('/usr/share/fonts/truetype/roboto/unhinted/RobotoTTF/Roboto-Regular.ttf', 30)

hack16 = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Regular.ttf', 16)
hack18 = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Regular.ttf', 18)
hack18bold = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Bold.ttf', 18)
hack20 = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Regular.ttf', 20)
hack24 = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Regular.ttf', 24)
hack36bold = ImageFont.truetype('/usr/share/fonts/truetype/hack/Hack-Bold.ttf', 36)

