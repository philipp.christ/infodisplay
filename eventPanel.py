'''
Draw a list of events for the upcoming 

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
import IDFont
from datetime import date, datetime, timedelta
import config
from icalevents.icalevents import events
import dateutil.parser


def drawEventPanel(width, height):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage)
    es = []

    maxDate = datetime.now() + timedelta(days=config.eventCalendar.get("eventDaysLimit"))
    try:
        es  = events(config.eventCalendarSec.get("url"), end=maxDate)
    except:
        print("error while retrieving event calendar ical")
    
    pwidth = 350
    pstartX = pstartY = 0
    pheight = 28
    rows = 10
    cols = 1
    for ev in sorted(es):
        #print(str(pstartX) + ":" + str(pstartY) + " " + str(ev))
        if pstartX >= cols:
            break

        singleDay = drawSingleDay(pwidth, pheight, ev)
        CurrentImage.paste(singleDay, (pstartX*pwidth, pstartY*pheight))

        if pstartY > 0 and pstartY % (rows-1) == 0:
            pstartY = 0
            pstartX += 1
            continue
        pstartY += 1

    return CurrentImage


def drawSingleDay(width, height, ev):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage)
    startDate = ev.start.strftime("%a %d.%m. ")
    title = ev.summary + " "

    twidth, theight = IDFont.robotoBold18.getsize(startDate)
    draw.text((110-twidth,0), startDate, font = IDFont.robotoBold18, fill = 0)

    twidth, theight = IDFont.robotoCondensed18.getsize(title)
    draw.text((120,0), title, font = IDFont.robotoCondensed18, fill = 0)
    
    return CurrentImage
