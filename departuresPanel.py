'''
Draw a list of departures for a specified public transport stop 

@package infodisplay
'''

from PIL import Image, ImageDraw, ImageFont
import IDFont
from datetime import date, datetime, timedelta
import config
import dateutil.parser
import departuresData


def drawDeparturesPanel(width, height):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage)

    departures = departuresData.getDepartures()
    
    lheight = 28
    rowPos = 8
    for dep in departures: 
        depline = drawSingleDeparture(width, lheight, dep)
        CurrentImage.paste(depline, (0, rowPos))
        rowPos += lheight

    return CurrentImage


def drawSingleDeparture(width, height, dep):
    CurrentImage = Image.new('1', (width, height), 255)
    draw = ImageDraw.Draw(CurrentImage)

    col = 10

    twidth, theight = IDFont.robotoBold18.getsize(dep["departureTime"])
    draw.text((col,int(height/2-theight/2)), dep["departureTime"], font = IDFont.roboto18, fill = 0)
    col += twidth + 6
    
    twidth, theight = IDFont.robotoBold18.getsize("+99")
    draw.text((col,int(height/2-theight/2)), dep["delay"], font = IDFont.robotoCondensed18, fill = 0)
    col += twidth

    draw.text((col,int(height/2-theight/2)), dep["line"], font = IDFont.robotoBold18, fill = 0)
    col += twidth
    draw.text((col,int(height/2-theight/2)), dep["destination"], font = IDFont.roboto18, fill = 0)

    
    return CurrentImage
